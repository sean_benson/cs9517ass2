'''
Created on 30 Mar 2015

@author: Sean
'''
import cv2
import numpy as np
from matplotlib import pyplot as plt
from cv2 import medianBlur
from numpy import dtype

def video(v, reference):
    cap = cv2.VideoCapture(v)
    if(cap.isOpened() == False): cap.open('test.avi')
    
#get reference frame if we need if
#cv2.imwrite('reference.png',ref)
    fourcc = cv2.cv.CV_FOURCC(*'DIVX')
    fps = int(cap.get(cv2.cv.CV_CAP_PROP_FPS))
    width = reference.shape[1] + int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
    
    height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
    
    out  = cv2.VideoWriter('tracked.avi',-1,fps,(width, height),3)
    print (width, height)
    print out.isOpened()
    while(True):
        ret, frame = cap.read()
        if not ret: break
    #if(ret): cv2.imshow('frame',frame)
    #cv2.waitKey(0)
        new_frame = f_match(reference, frame)
        
        #get affine transform
        trans = transform(new_frame[1],new_frame[2])
        #draw rectangle
        if trans is None: #can't find the image so do nothing except write frame
            out.write(new_frame[0])
            continue
        ul = np.array([[1],[1],[1]])
        ur = np.array([[reference.shape[1]],[1],[1]])
        br = np.array([[reference.shape[1]],[reference.shape[0]],[1]])
        bl = np.array([[1],[reference.shape[0]],[1]])
        ul = np.dot(trans,ul)
        br = np.dot(trans,br)
        ur = np.dot(trans,ur)
        bl = np.dot(trans,bl)
        ul = (int(ul[0]),int(ul[1]))
        br = (int(br[0]),int(br[1]))
        ur = (int(ur[0]),int(ur[1]))
        bl = (int(bl[0]),int(bl[1]))
        #cv2.rectangle(new_frame[0],ul,bl,(255,0,0),5)
        cv2.line(new_frame[0], ul, ur, (255,0,0),5)
        cv2.line(new_frame[0], ul, bl, (255,0,0),5)
        cv2.line(new_frame[0], ur, br, (255,0,0),5)
        cv2.line(new_frame[0], bl, br, (255,0,0),5)
        out.write(new_frame[0])
        
        
    cap.release()    
    out.release()
    
def f_match(ref, scene):
    
    img1 = ref
    img2 = scene
    #img1 = cv2.GaussianBlur(ref,(19,9),0)
    #img2 = cv2.GaussianBlur(scene,(19,9),0)
    #img1 = cv2.medianBlur(ref,9)
    #img2 = cv2.medianBlur(scene,9)
    # Initiate SIFT detector
    sift = cv2.SIFT()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)

    # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)  # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1, des2, k=2)



# ratio test as per Lowe's paper
    kp = list()
    
    for i, (m, n) in enumerate(matches):
        if m.distance < 0.7 * n.distance:
           kp.append((kp1[m.queryIdx], kp2[m.trainIdx]))     

    #horizantally stack images
    href, wref = ref.shape[:2]
    hframe, wframe = scene.shape[:2]
    comb = np.zeros((max(href,hframe), wframe + wref, 3), np.uint8)
    comb[:href,:wref,:3] = ref
    comb[:hframe,wref:wref+wframe,:3] = scene
    #draw points
    
    for k in kp:
        k[1].pt = (k[1].pt[0]+wref,k[1].pt[1])
    #kp = integerise(kp)
    #cv2.imshow("h",comb)
    #cv2.waitKey(0)
    #draw features on img 1
    #get separate kp lists again
    refkp = list()
    framekp = list()
    for x in kp:
        refkp.append(x[0])
        framekp.append(x[1])
    
    #comb=cv2.drawKeypoints(comb,refkp)
    #comb=cv2.drawKeypoints(comb,framekp)
    '''count = 0
    
    for l in kp:
        cv2.line(comb, (int(refkp[count].pt[0]),int(refkp[count].pt[1])),(int(framekp[count].pt[0]),int(framekp[count].pt[1])), (255,0,0),1)
        
        count = count+1
    '''
    return comb, refkp, framekp
    
    
    #NEED TO FIX - this function isn't on my installed version of openCV - Sean
    #img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, matches, None, **draw_params)

    #plt.imshow(img3,), plt.show()
    
def integerise(kps):
    for k in kps:
        k[0].pt = (int(k[0].pt[0]),int(k[0].pt[1]))
        k[1].pt = (int(k[1].pt[0]),int(k[1].pt[1]))
    return kps

#f = open('test-mpeg_512kb.mp4', 'r')

def transform(refkp, framekp):
    #add the extra [] to make 3D array
    if refkp is None: return None
    if framekp is None: return None
    #if less than 5 matches then we can't find a transform
    if len(refkp) < 5: return None
    refkp = np.array([coords_from_kp(refkp)],dtype = 'float32')
    framekp = np.array([coords_from_kp(framekp)], dtype = 'float32')
    
    
    trs = cv2.estimateRigidTransform(refkp,framekp,True)
    return trs
    

def coords_from_kp(kp):
    new = list()
    for k in kp:
        new.append([(k.pt[0]), (k.pt[1])])
    return new
