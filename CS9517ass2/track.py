'''
Created on 2 Apr 2015

@author: Sean
'''

import cv2
import tracker as tk

import objectSelector as obj
import display as d
from copy import deepcopy
import argparse
import datetime

parser = argparse.ArgumentParser(description='Track object in a video')
parser.add_argument('video', help='choose a video file')
parser.add_argument('size', type=float, help='specify the down-sampling of video for run speed < 1')
parser.add_argument('--realtime', help='display video during processing')
parser.add_argument('--camera', help='use the camera on device')
parser.add_argument('output',help="output file name")
args = parser.parse_args()
video = args.video
size = args.size
output_file = args.output
if(args.camera): video = 0
#open video
cap = cv2.VideoCapture(video)
#get first frame
ret, first = cap.read() 

reflist = obj.choose(first)



#scan all reference objects for descriptors and kps
refdeskps = list()
for ref in reflist:
    refdeskps.append(tk.scan_ref(ref))
#main loop, scan video frames
#cap = cv2.VideoCapture(video)
fourcc = cv2.cv.CV_FOURCC(*'DIVX')
fps = int(cap.get(cv2.cv.CV_CAP_PROP_FPS))

width, height = d.get_video_frame_size(reflist, first) 

out  = cv2.VideoWriter(output_file,-1,fps,(width, height),3)
while(True):
    
    if(args.realtime):
        #need to know how long the processing is taking so we can display the image for the right amount of time
        start_time = datetime.datetime.now()
           
    ret, scene = cap.read()
    if not ret: break
    
    #scan scene for kps and des
    scenekps, scenedes = tk.scan_scene(scene, size)
    #get matches list for each ref
    matches_kp = list()
    for r in refdeskps:
        refdes = r[1]
        refkps = r[0]
        matches_kp.append(tk.f_match(refdes, refkps, scenedes, scenekps))
        
    #get transforms
    transforms = list()
    ref_kp_list_xy = list()
    scene_kp_list_xy = list()
    count = 0
    for m in matches_kp:
        count = count + 1
        if len(m) < 2:
            transforms.append(None)
            continue
        
        
        ref_xy, scene_xy = tk.split_kps_to_ref_frame(m)
        
        transforms.append(tk.transform(ref_xy, scene_xy))
        ref_kp_list_xy.append(tk.kp_to_xy(ref_xy, 1))
        scene_kp_list_xy.append(tk.kp_to_xy(scene_xy, size))
        
        
    #get corners of objects in the scene
    corners = list()
    for idx, ref in enumerate(reflist):
        corner = tk.get_corners(ref, transforms[idx],size)
        corners.append(corner)
    #need a clone for the reference pics
    ref_draws = deepcopy(reflist)       
    #draw kps on refs and scene
    d.draw_kps(ref_kp_list_xy, ref_draws, scene_kp_list_xy, scene)
    d.draw_polys(scene, corners)
    
    comb = d.combined_ref_with_scene(scene, ref_draws)
    
    if(args.realtime):
        end_time = datetime.datetime.now()
        delay = end_time - start_time
        cv2.imshow("video", comb)
        
        key = cv2.waitKey(40) & 0xFF
        if key == ord('q'):break
    out.write(comb)
