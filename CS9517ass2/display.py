'''
Created on 9 Apr 2015

@author: Sean
'''

import cv2
import numpy as np

#global variable colours for interating through BGR when drawing
colours = [(255,0,0),(0,255,0),(0,0,255)]

def combined_ref_with_scene(scene, refs):
    #places reference images on the left of a scene.
    #get height and width
    height = 0
    width = 0
    for image in refs:
        height = height + image.shape[0]
        width = max(width, image.shape[1])
    ref_width = width
    width = width + scene.shape[1]
    height = max(height, scene.shape[0])
    comb = np.zeros((height, width, 3), np.uint8)
    #insert scene 
    comb[:min(height, scene.shape[0]),ref_width:,:3] = scene
    next_h = 0
    #insert reference pics
    for image in refs:
        h = image.shape[0]
        w = image.shape[1]
        comb[next_h:next_h + h,:w,:3] = image
        next_h = h + next_h
    return comb

def draw_kps(refkps, refs, scenekps, scene):
    #kps are series of x,y coords
    global colours
    
    for idx, rkp in enumerate(refkps):
        for kpidx, point in enumerate(rkp):
            #draw points on ref
            
            cv2.circle(refs[idx], point, 2, colours[idx%3], 1)
            #corresponding poinr on the scene in same colour
            cv2.circle(scene, scenekps[idx][kpidx], 2, colours[idx%3], 1)
   

#corners is list of coordinates of the corners in drawing order for tracking trajectories
def draw_polys(scene, corners_list):
    
    for colour, corners in enumerate(corners_list):
        if not(corners): continue
        #transpose to correnct place
        for idx, corner in enumerate(corners):
            #check if last corner so we connect to the beginning.
            if idx == len(corners) - 1:
                cv2.line(scene, corners[idx], corners[0], colours[colour%3],3)
            else:
                cv2.line(scene, corners[idx], corners[idx+1], colours[colour%3],3)
            
def max_width(images):
    width = 0
    for image in images:
        width = max(width, image.shape[1])
    return width

def get_video_frame_size(reflist, scene):
    #get max wifth of refs, then add scene width
    width = max_width(reflist) + scene.shape[1]
    refheight = 0
    for ref in reflist:
        refheight = refheight + ref.shape[0]
    height = max(refheight, scene.shape[0])
    return width, height


    
        